import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class SignUpPage extends Page {
    /**
     * define selectors using getter methods
     */
    get lnkShoe () { return $('a[href="/product/nmd_r1-spectoo-shoes-black"]') };
    get divSizeSelect () { return $('[data-testid="product-order-select-size"]') };
    get optFirst () { return $('div[class="selected item"]') };
    get btnAddBasket () { return $('button[data-testid="product-order-button"]') };
    get lnkOrderNow () { return $('[data-testid="order-now-button"]') };
    get lnkCheckout () { return $('a[class="action-featured-black proceed-to-checkout action-fill-black"]') };

    async checkoutItem() {
        await this.lnkShoe.click();
        await this.divSizeSelect.click();
        await this.optFirst.click();
        await this.btnAddBasket.click();
        await this.lnkOrderNow.click();
        await this.lnkCheckout.click();
    }

}

export default new SignUpPage();
