import Page from "./page";

/**
 * sub page containing specific selectors and methods for a specific page
 */
class AllBrandsPage extends Page {
  /**
   * define selectors using getter methods
   */
  get spanToggleSearch() {
    return $('[class="header toggle search"]');
  }
  get inputSearch() {
    return $('input[name="search-brands"]');
  }
  get btnSearch() {
    return $('[class="brands-btn btn action-featured-black"]');
  }
  get btnShopNow() {
    return $('a[data-saleslanding="https://www.otrium.com/outlet/adidas/"]');
  }
  check = (brand) => $(`[data-name="${brand}"]`);

  async searchBrand(brand) {
    await this.spanToggleSearch.click();
    await this.inputSearch.setValue(brand);
    await this.check(brand).click();
    await this.btnSearch.click();
  }
}

export default new AllBrandsPage();
