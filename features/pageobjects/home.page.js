import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class HomePage extends Page {
    /**
     * define selectors using getter methods
     */
    get linkAllBrands () { return $('a[href="/all-brands"]') }
}

export default new HomePage();
