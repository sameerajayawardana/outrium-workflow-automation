import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class SignUpPage extends Page {
    /**
     * define selectors using getter methods
     */
    get lnkMen () { return $('//span[text()="men"]') }
    get inputEmail () { return $('[name="email"]') }
    get btnCreateAcc () { return $('button[type="submit"]') }

    async signUp() {
        await this.lnkMen.click();
        await this.inputEmail.setValue('test@mail.com');
        await this.btnCreateAcc.click();
    }

}

export default new SignUpPage();
