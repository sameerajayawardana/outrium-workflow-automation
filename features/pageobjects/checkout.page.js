import Page from "./page";

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CheckoutPage extends Page {
  /**
   * define selectors using getter methods
   */
  get inputFirstName() {
    return $("#shipping_first_name");
  }
  get inputLastName() {
    return $("#shipping_last_name");
  }
  get inputEmailRepeat() {
    return $("#shipping_email_repeat");
  }
  get inputPostal() {
    return $("#shipping_postcode");
  }
  get inputHouseNumber() {
    return $("#shipping_house_number");
  }
  get inputCity() {
    return $("#shipping_city");
  }
  get inputStreet() {
    return $("#shipping_street_name");
  }
  get btnOrderNow() {
    return $('button[class="checkout-btn-cnt-new active continue-checkout"]');
  }
  get divPayment() { return $('#payment') }
  get btnPlaceOrder() { return $('#place_order') }

  async fillShippingDetails() {
    await this.inputPostal.setValue("1051 LG");
    await this.inputHouseNumber.setValue("1111");
    await this.inputFirstName.setValue("James");
    await this.inputLastName.setValue("Camaron");
    await this.inputEmailRepeat.setValue("test@mail.com");
    await browser.pause(1500);
    await this.inputCity.setValue("Danzigerkade");
    await this.inputStreet.setValue("street 1");
    await this.btnOrderNow.click();
  }
}

export default new CheckoutPage();
