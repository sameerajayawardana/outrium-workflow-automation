Feature: Otrium purchase workflow

  Scenario Outline: As an unauthenticated user, I should be able to buy an Adidas product

    Given I am on the Otrium home page
    When I try to buy an adidas product
    When I sign up with to the site
    Then I should be able to purchase
