import { Given, When, Then } from '@cucumber/cucumber';

import HomePage from '../pageobjects/home.page';
import SignUpPage from '../pageobjects/signUp.page';
import AllBrandsPage from '../pageobjects/allBrands.page';
import AdidasPage from '../pageobjects/adidas.page';
import CheckoutPage from '../pageobjects/checkout.page';

Given('I am on the Otrium home page', async () => {
    await HomePage.open()
    await HomePage.linkAllBrands.click();
});

When(/^I try to buy an (\w+) product/, async (product) => {
    await AllBrandsPage.searchBrand(product);
    await AllBrandsPage.btnShopNow.click();
});

When('I sign up with to the site', async () => {
    await SignUpPage.signUp();
})

Then('I should be able to purchase', async () => {
    await AdidasPage.checkoutItem();
    await CheckoutPage.fillShippingDetails();

    await expect(CheckoutPage.divPayment).toBeExisting();
    await expect(CheckoutPage.btnPlaceOrder).toBeEnabled();
});



