# Otrium purchase workflow automation

This project was created with WebdriverIO to automate non-authenticated user product purchase workflow

## Running Tests
Used tools
1. WebdriverIO
2. Cucumber
3. Allure Reporting

### Running tests

All the tests in this project can be executed with the command below

`npm test`

### Generating test results

End to end test results can be generated through Allure HTML reporting with the command below.

`npm run allure-generate`